***Canen and Martin (2020) - RDD implementation***

*To install the CCT codes, use ssc install rddensity, ssc install lpdensity, ssc install rdrobust and find them in the searches.
*For McCrary test, please save the .ado file downloaded from https://eml.berkeley.edu/~jmccrary/DCdensity/ in your ado folder

clear

*Set Directory
cd "data"

*Load Data
*NOTE: this file exceeds gitlab storage limit, not included in the archive. Can be created by converting data/all_rd_data.rds to Stata format
use rdd_adsdata.dta

*Data Treatment
drop if group=="C1"
gen outcome = news_post - news_pre

***Local linear (p=1), CCT

*Bandwidth: 1 minute
rdrobust outcome tunein_delta, h(60)

*Bandwidth: 5 minutes
rdrobust outcome tunein_delta, h(300)

*Bandwidth: 10 minutes
*rdrobust outcome tunein_delta, h(600)

*CCT estimates - optimal bandwidth
rdrobust outcome tunein_delta


***Local linear (p=2), CCT

*Bandwidth: 1 minute
rdrobust outcome tunein_delta, p(2) h(60)

*Bandwidth: 5 minutes
rdrobust outcome tunein_delta, p(2) h(300)

*Bandwidth: 10 minutes
*rdrobust outcome tunein_delta, p(2) h(600)

*CCT estimates - optimal bandwidth
rdrobust outcome tunein_delta, p(2)
