###
### for each device on each day, compute
### future ads exposure
### past ads exposure
### run gen_t_c_groups.R first

library(dplyr)
library(purrr)
library(tidyr)
library(forcats)
library(stringr)
library(readr)
library(tibble)
library(data.table)
library(parallel)
library(lubridate)
library(hms)
library(magrittr)

setDTthreads(1)

### SET WORKING DIRECTORY HERE 
path_to_archive <- "replication/"
setwd(path_to_archive)

# first compute ads exposure by device
ad_exp_pre_post <- function(ad_times, window = 24) {
	n<- length(ad_times)
	if(n==1) return(0)
	if(n==2){
		if (ad_times[2] < ad_times[1] + hours(window)) return(c(1,-1))
		else return(c(0,0))
	}
	c(sum(ad_times[2:n] < ad_times[1] + hours(window)),
	map_dbl(seq_along(ad_times)[2:(n-1)], 
			~ sum(ad_times[(.+1):n] < ad_times[.] + hours(window)) - sum(ad_times[1:(.-1)] > ad_times[.] - hours(window))),
	-sum(ad_times[1:(n-1)] > ad_times[n] - hours(window)))
}

get_daily_ads <- function(d) {
	cat(as.character(d), "\n")
	
	ads_yday <- readRDS(paste("data/dd/tc_groups_", as.character(d-1), ".rds", sep="")) %>%
	 filter(map_dbl(t_c, nrow) > 0) %>%
	 unnest %>%
	 as.data.table

	ads_tday <- readRDS(paste("data/dd/tc_groups_", as.character(d), ".rds", sep="")) %>%
	 filter(map_dbl(t_c, nrow) > 0) %>%
	 unnest %>%
	 as.data.table

	ads_tmw <- readRDS(paste("data/dd/tc_groups_", as.character(d-1), ".rds", sep="")) %>%
	 filter(map_dbl(t_c, nrow) > 0) %>%
	 unnest %>%
	 as.data.table

	all_ads <- rbind(ads_yday, ads_tday, ads_tmw) %>% setkey(device_id, event_time_utc)
	
	all_ads[,exp_diff:=ad_exp_pre_post(event_time_utc), by = .(device_id)] %>%
		  .[ads_tday, on = c("dma_code", "channel", "affiliate", "program", "event_time_utc", "device_id", "group")] %>%
		  .[,.(exp_diff=mean(exp_diff), n=.N),by=.(dma_code, channel, affiliate, program, ad_time_utc=event_time_utc, group)]

}


dates <- seq(mdy("09/02/2012"), mdy("11/05/2012"), by = "days")
all_exposure <- dates %>% 
	mclapply(FUN=get_daily_ads, mc.cores=32, mc.preschedule=F) %>% 
	rbindlist %>%
	saveRDS("data/all_dd_data_exposure.rds")
